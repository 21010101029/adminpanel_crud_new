﻿using System.ComponentModel.DataAnnotations;

namespace AdminPanel_New.Models
{
    public class SearchModel
    {
        public int? CityID { get; set; }
        public int? CountryID { get; set; }
        public int? StateID { get; set; }
        public string CityName { get; set; }

        [Required]
        public string CityCode { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        [Required]
        public string CountryName { get; set; }
        [Required]
        public string StateName { get; set; }
        public string StateCode { get; set; }
    }
}
