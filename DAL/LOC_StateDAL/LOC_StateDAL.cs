﻿using AdminPanel_New.Areas.Country.Models;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;
using System.Data;
using AdminPanel_New.Areas.City.Models;
using AdminPanel_New.Areas.States.Models;

namespace AdminPanel_New.DAL.LOC_StateDAL
{
    public class LOC_StateDAL : LOC_StateDALBase
    {
        #region dbo_PR_LOC_Country_SelectComboBox
        public List<LOC_CountryDropDown> DropdownCountry()
        {
            try
            {
                SqlDatabase sqlDatabase = new SqlDatabase(myConnectionString);
                DbCommand dbCommand = sqlDatabase.GetStoredProcCommand("PR_LOC_Country_SelectComboBox");
                DataTable dt = new DataTable();
                using (IDataReader reader = sqlDatabase.ExecuteReader(dbCommand))
                {
                    dt.Load(reader);
                }
                List<LOC_CountryDropDown> ListOfCountry = new List<LOC_CountryDropDown>();
                foreach (DataRow dr in dt.Rows)
                {
                    LOC_CountryDropDown lOC_CountryDropDown = new LOC_CountryDropDown();
                    lOC_CountryDropDown.CountryID = Convert.ToInt32(dr["CountryID"]);
                    lOC_CountryDropDown.CountryName = dr["CountryName"].ToString();
                    ListOfCountry.Add(lOC_CountryDropDown);
                }
                return ListOfCountry;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion

        #region dbo_PR_LOC_deleteCity_FollowedBy_States
        public void PR_LOC_deleteCity_FollowedBy_States(int? StateID)
        {
            try
            {
                SqlDatabase sqlDatabase = new SqlDatabase(myConnectionString);
                DbCommand dbCommand = sqlDatabase.GetStoredProcCommand("PR_LOC_deleteCity_FollowedBy_States");
                sqlDatabase.AddInParameter(dbCommand, "@StateID", DbType.Int64, StateID);
                sqlDatabase.ExecuteNonQuery(dbCommand);
                return;
            }
            catch (Exception ex)
            {
                return;
            }
        }
        #endregion

        #region dbo_PR_State_SelectByStateName
        public DataTable PR_State_SelectByStateName(string StateName)
        {
            try
            {
                SqlDatabase sqlDatabase = new SqlDatabase(myConnectionString);
                DbCommand dbCommand = sqlDatabase.GetStoredProcCommand("PR_State_SelectByStateName");
                Console.WriteLine(StateName);
                sqlDatabase.AddInParameter(dbCommand, "@SName", DbType.String, StateName);
                // sqlDatabase.AddInParameter(dbCommand, "@CountryID", DbType.Int64 , LOC_StateModel.CountryID);
                DataTable dt = new DataTable();
                using (IDataReader reader = sqlDatabase.ExecuteReader(dbCommand))
                {
                    dt.Load(reader);
                }
                return dt;
            }
            catch (Exception e) { return null; }
        }
        #endregion

    }
}
