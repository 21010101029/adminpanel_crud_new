﻿using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;
using System.Data;
using AdminPanel_New.Areas.Country.Models;
using AdminPanel_New.Areas.States.Models;
using System.Drawing.Printing;

namespace AdminPanel_New.DAL.LOC_StateDAL
{
    public class LOC_StateDALBase:DALHelper
    {
        LOC_StateModel model= new LOC_StateModel();
        #region dbo_PR_LOC_State_SelectAll
        public DataTable dbo_PR_LOC_State_SelectAll()
        {
            try
            {
                SqlDatabase sqlDatabase = new SqlDatabase(myConnectionString);
                DbCommand dbCommand = sqlDatabase.GetStoredProcCommand("PR_LOC_State_SelectAll");
                DataTable dt = new DataTable();
                using (IDataReader reader = sqlDatabase.ExecuteReader(dbCommand))
                {
                    dt.Load(reader);
                }
                return dt;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion

        #region dbo_PR_LOC_State_Insert & PR_LOC_State_Update
        public bool SaveState(LOC_StateModel modelLOC_StateModel)
        {
            try
            {
                if (modelLOC_StateModel.StateID == 0)
                {
                    SqlDatabase sqlDatabase = new SqlDatabase(myConnectionString);
                    DbCommand dbCommand = sqlDatabase.GetStoredProcCommand("PR_LOC_State_Insert");
                    sqlDatabase.AddInParameter(dbCommand, "@StateName", DbType.String, modelLOC_StateModel.StateName);
                    sqlDatabase.AddInParameter(dbCommand, "@StateCode", DbType.String, modelLOC_StateModel.StateCode);
                    sqlDatabase.AddInParameter(dbCommand, "@CountryID", DbType.Int64, modelLOC_StateModel.CountryID);
                    sqlDatabase.AddInParameter(dbCommand, "@Created", DbType.DateTime, DBNull.Value);
                    sqlDatabase.AddInParameter(dbCommand, "@Modified", DbType.DateTime, DBNull.Value);
                    sqlDatabase.ExecuteNonQuery(dbCommand);
                    return true;

                }
                else
                {
                    SqlDatabase sqlDatabase = new SqlDatabase(myConnectionString);
                    DbCommand dbCommand = sqlDatabase.GetStoredProcCommand("PR_LOC_State_Update");
                    sqlDatabase.AddInParameter(dbCommand, "@StateID", DbType.Int64, modelLOC_StateModel.StateID);
                    sqlDatabase.AddInParameter(dbCommand, "@StateName", DbType.String, modelLOC_StateModel.StateName);
                    sqlDatabase.AddInParameter(dbCommand, "@StateCode", DbType.String, modelLOC_StateModel.StateCode);
                    sqlDatabase.AddInParameter(dbCommand, "@CountryID", DbType.Int64, modelLOC_StateModel.CountryID);
                    sqlDatabase.AddInParameter(dbCommand, "@Modified", DbType.DateTime, DBNull.Value);
                    sqlDatabase.ExecuteNonQuery(dbCommand);
                    return true;
                }
            }catch(Exception ex)
            {
                return false;
            }

        }
        #endregion

        #region dbo_PR_LOC_State_Delete
        public void PR_LOC_State_Delete(int ? stateID)
        {
            if(stateID == null)
            {
                return;
            }
            try
            {
                SqlDatabase sqlDatabase = new SqlDatabase(myConnectionString);
                DbCommand dbCommand = sqlDatabase.GetStoredProcCommand("PR_LOC_State_Delete");
                sqlDatabase.AddInParameter(dbCommand, "@StateID", DbType.Int64, stateID);
                sqlDatabase.ExecuteNonQuery(dbCommand);
                return;
            }catch(Exception ex)
            {

            }


        }
        #endregion

        #region dbo_PR_LOC_State_SelectByPK

        public LOC_StateModel PR_LOC_State_SelectByPK(int? StateID)
        {
            Console.WriteLine(StateID.ToString());
            SqlDatabase sqlDatabase= new SqlDatabase(myConnectionString);
            DbCommand dbCommand = sqlDatabase.GetStoredProcCommand("PR_LOC_State_SelectByPK");
            sqlDatabase.AddInParameter(dbCommand, "@StateID",DbType.Int64, StateID);
            DataTable dt = new DataTable();
            using (IDataReader  reader = sqlDatabase.ExecuteReader(dbCommand))
            {
                dt.Load(reader);
            }
            foreach(DataRow dr in dt.Rows)
            {
                model.StateID = Convert.ToInt32(dr["StateID"]);
                Console.WriteLine(model.StateID);
                model.StateName = dr["StateName"].ToString();
                Console.WriteLine(model.StateName);
                model.StateCode = dr["StateCode"].ToString();
                Console.WriteLine(model.StateCode);
                model.Created = Convert.ToDateTime(dr["Created"].ToString());
                Console.WriteLine(model.Created);
                model.Modified = Convert.ToDateTime(dr["Modified"].ToString());
                Console.WriteLine(model.Modified);
                model.CountryID = Convert.ToInt32(dr["CountryID"]);
                Console.WriteLine(model.CountryID);
            }
            return model;
        }
        #endregion

    }
}
