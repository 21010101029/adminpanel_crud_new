﻿namespace AdminPanel_New.DAL
{
    public class DALHelper
    {
        #region Connection String Of Database
        public static string myConnectionString = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetConnectionString("myConnectionString");
        #endregion
    }
}
