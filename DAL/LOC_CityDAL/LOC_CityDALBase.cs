﻿using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;
using System.Data;
using AdminPanel_New.Areas.City.Models;
using System.Reflection;

namespace AdminPanel_New.DAL.LOC_CityDAL
{
    public class LOC_CityDALBase:DALHelper
    {
        #region dbo_PR_LOC_City_SelectAll
        public DataTable dbo_PR_LOC_City_SelectAll()
        {
            try
            {
                SqlDatabase sqlDatabase = new SqlDatabase(myConnectionString);
                DbCommand dbCommand = sqlDatabase.GetStoredProcCommand("PR_LOC_City_SelectAll");
                DataTable dt = new DataTable();
                using (IDataReader reader = sqlDatabase.ExecuteReader(dbCommand))
                {
                    dt.Load(reader);
                }
                return dt;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion

        #region dbo_PR_LOC_City_Insert & PR_LOC_City_Update
        public bool SaveCity(LOC_CityModel model)
        {
            try
            {
                //Console.WriteLine("CityID "model.CityID);
                if(model.CityID == null)
                {
                    SqlDatabase sqlDatabase = new SqlDatabase(myConnectionString);
                    DbCommand dbCommand = sqlDatabase.GetStoredProcCommand("PR_LOC_City_Insert");
                    sqlDatabase.AddInParameter(dbCommand, "@CityName", DbType.String, model.CityName);
                    sqlDatabase.AddInParameter(dbCommand, "@CityCode", DbType.String, model.CityCode);
                    sqlDatabase.AddInParameter(dbCommand, "@CountryID", DbType.Int64, model.CountryID);
                    sqlDatabase.AddInParameter(dbCommand, "@StateID", DbType.Int64, model.StateID);
                    sqlDatabase.AddInParameter(dbCommand, "@Created", DbType.DateTime, DBNull.Value);
                    sqlDatabase.AddInParameter(dbCommand, "@Modified", DbType.String, DBNull.Value);
                    sqlDatabase.ExecuteNonQuery(dbCommand);
                    return true;
                }
                else
                {
                    SqlDatabase sqlDatabase = new SqlDatabase(myConnectionString);
                    DbCommand dbCommand = sqlDatabase.GetStoredProcCommand("PR_LOC_City_Update");
                    Console.WriteLine("Hello "+model.CityID);
                    sqlDatabase.AddInParameter(dbCommand, "@CityID", DbType.Int64, model.CityID);
                    sqlDatabase.AddInParameter(dbCommand, "@StateID", DbType.Int64, model.StateID);
                    sqlDatabase.AddInParameter(dbCommand, "@CityName", DbType.String, model.CityName);
                    sqlDatabase.AddInParameter(dbCommand, "@CityCode", DbType.String, model.CityCode);
                    sqlDatabase.AddInParameter(dbCommand, "@CountryID", DbType.Int64, model.CountryID);
                    sqlDatabase.AddInParameter(dbCommand, "@Modified", DbType.String, DBNull.Value);
                    sqlDatabase.ExecuteNonQuery(dbCommand);
                    return true;
                }
                
            }
            catch (Exception ex) 
            {
                return false;
            }
        }
        #endregion

        #region dbo_PR_LOC_City_SelectByPK
        public LOC_CityModel PR_LOC_City_SelectByPK(int? CityID)
        {
            LOC_CityModel model=new LOC_CityModel();
            SqlDatabase sqlDatabase = new SqlDatabase(myConnectionString);
            DbCommand dbCommand = sqlDatabase.GetStoredProcCommand("PR_LOC_City_SelectByPK");
            sqlDatabase.AddInParameter(dbCommand, "@CityID", DbType.Int64, CityID);
            DataTable dt = new DataTable();
            using (IDataReader reader = sqlDatabase.ExecuteReader(dbCommand))
            {
                dt.Load(reader);
            }
            foreach (DataRow dr in dt.Rows)
            {
                model.StateID = Convert.ToInt32(dr["StateID"]);
                model.Created = Convert.ToDateTime(dr["Created"].ToString());
                model.Modified = Convert.ToDateTime(dr["Modified"].ToString());
                model.CountryID = Convert.ToInt32(dr["CountryID"]);
                model.CityID = Convert.ToInt32(dr["CityID"]);
                model.CityCode = dr["CityCode"].ToString();
                model.CityName = dr["CityName"].ToString();
            }
            return model;
        }
        #endregion

        #region dbo_PR_LOC_City_Delete
        public void PR_LOC_City_Delete(int? CityID)
        {
            try
            {
                Console.WriteLine("Hello"+CityID.ToString());
                SqlDatabase sqlDatabase=new SqlDatabase(myConnectionString);
                DbCommand dbCommand = sqlDatabase.GetStoredProcCommand("PR_LOC_City_Delete");
                sqlDatabase.AddInParameter(dbCommand, "@CityID", DbType.Int64, CityID);
                sqlDatabase.ExecuteNonQuery(dbCommand);
                return;
            }
            catch (Exception ex)
            {

            }
        }
        #endregion
    }
}
