﻿using AdminPanel_New.Areas.Student.Models;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data;
using System.Data.Common;

namespace AdminPanel_New.DAL.MST_StudentDAL
{
    public class MST_StudentDALBase:DALHelper
    {
        MST_StudentModel model= new MST_StudentModel(); 
        #region [dbo].[PR_Student_SelectAll]
        public DataTable PR_Student_SelectAll()
        {
            try
            {
                SqlDatabase sqlDatabase = new SqlDatabase(myConnectionString);
                DbCommand dbCommand = sqlDatabase.GetStoredProcCommand("PR_Student_SelectAll");
                DataTable dt=new DataTable();
                using (IDataReader reader = sqlDatabase.ExecuteReader(dbCommand))
                {
                    dt.Load(reader);
                }
                return dt;


            }
            catch (Exception ex)
            {
                return null;

            }
        }
        #endregion

        #region [dbo].[PR_Student_DeleteByPK]
        public void PR_Student_DeleteByPK(int ? StudentID)
        {
            try
            {
                SqlDatabase sqlDatabase = new SqlDatabase(myConnectionString);
                DbCommand dbCommand = sqlDatabase.GetStoredProcCommand("PR_Student_DeleteByPK");
                sqlDatabase.AddInParameter(dbCommand, "@StudentID", DbType.Int32, StudentID);
                sqlDatabase.ExecuteNonQuery(dbCommand);
                return;
            }
            catch(Exception ex)
            {
                return;
            }
                
        }
        #endregion

        #region [dbo].[PR_Student_SelectByPk]
        public MST_StudentModel PR_Student_SelectByPk(int ? StudentID)
        {
            try {
                SqlDatabase sqlDatabase = new SqlDatabase(myConnectionString);
                DbCommand dbCommand = sqlDatabase.GetStoredProcCommand("PR_Student_SelectByPk");
                sqlDatabase.AddInParameter(dbCommand, "@StudentID", DbType.Int32, StudentID);
                DataTable dt = new DataTable();
                using (IDataReader reader = sqlDatabase.ExecuteReader(dbCommand)) { dt.Load(reader); }
                foreach (DataRow dr in dt.Rows)
                {
                    model.StudentID = Convert.ToInt32(dr["StudentID"]);
                    model.StudentName = dr["StudentName"].ToString();
                    model.BirthDate = Convert.ToDateTime(dr["BirthDate"].ToString());
                    model.Age = Convert.ToInt32(dr["Age"].ToString());
                    model.MobileNoStudent = dr["MobileNoStudent"].ToString();
                    model.MobileNoFather = dr["MobileNoFather"].ToString();
                    model.Email = dr["Email"].ToString();
                    model.Address = dr["Address"].ToString();
                    model.CityID = Convert.ToInt32(dr["CityID"].ToString());
                    model.CityName = dr["CityName"].ToString();
                    model.Gender = dr["Gender"].ToString();
                    model.Password = dr["Password"].ToString();
                    model.BranchID = Convert.ToInt32(dr["BranchID"].ToString());
                    model.BranchName = dr["BranchName"].ToString();
                    model.IsActive = Convert.ToBoolean(dr["IsActive"].ToString());
                }
                return model;
            }catch (Exception ex) {
                return null;           
            }
           
        }
        #endregion

        #region [dbo].[PR_Student_Insert] && [dbo].[PR_Student_UpdateByPK]
        public bool Save(MST_StudentModel model)
        {
            try
            {
                if(model.StudentID == 0)
                {
                    SqlDatabase sqlDatabase = new SqlDatabase(myConnectionString);
                    DbCommand dbCommand = sqlDatabase.GetStoredProcCommand("PR_Student_Insert");
                    sqlDatabase.AddInParameter(dbCommand, "@BranchID", DbType.Int32, model.BranchID);
                    Console.WriteLine(model.BranchID);
                    sqlDatabase.AddInParameter(dbCommand, "@CityID", DbType.Int32, model.CityID);
                    Console.WriteLine(model.CityID);

                    sqlDatabase.AddInParameter(dbCommand, "@StudentName", DbType.String, model.StudentName);
                    sqlDatabase.AddInParameter(dbCommand, "@MobileNoStudent", DbType.String, model.MobileNoStudent);
                    sqlDatabase.AddInParameter(dbCommand, "@MobileNoFather", DbType.String, model.MobileNoFather);
                    sqlDatabase.AddInParameter(dbCommand, "@Email", DbType.String, model.Email);
                    sqlDatabase.AddInParameter(dbCommand, "@Address", DbType.String, model.Address);
                    sqlDatabase.AddInParameter(dbCommand, "@BirthDate", DbType.DateTime, model.BirthDate);
                    sqlDatabase.AddInParameter(dbCommand, "@Age", DbType.Int32, model.Age);
                    sqlDatabase.AddInParameter(dbCommand, "@IsActive", DbType.Boolean, model.IsActive);
                    sqlDatabase.AddInParameter(dbCommand, "@Gender", DbType.String, model.Gender);
                    sqlDatabase.AddInParameter(dbCommand, "@Password", DbType.String, model.Password);
                    sqlDatabase.ExecuteNonQuery(dbCommand);
                    return true;
                }
                else
                {
                    SqlDatabase sqlDatabase = new SqlDatabase(myConnectionString);
                    DbCommand dbCommand = sqlDatabase.GetStoredProcCommand("PR_Student_UpdateByPK");
                    sqlDatabase.AddInParameter(dbCommand, "@BranchID", DbType.Int32, model.BranchID);
                    sqlDatabase.AddInParameter(dbCommand, "@StudentID", DbType.Int32, model.StudentID);
                    sqlDatabase.AddInParameter(dbCommand, "@CityID", DbType.Int32, model.CityID);
                    sqlDatabase.AddInParameter(dbCommand, "@StudentName", DbType.String, model.StudentName);
                    sqlDatabase.AddInParameter(dbCommand, "@MobileNoStudent", DbType.String, model.MobileNoStudent);
                    sqlDatabase.AddInParameter(dbCommand, "@MobileNoFather", DbType.String, model.MobileNoFather);
                    sqlDatabase.AddInParameter(dbCommand, "@Email", DbType.String, model.Email);
                    sqlDatabase.AddInParameter(dbCommand, "@Address", DbType.String, model.Address);
                    sqlDatabase.AddInParameter(dbCommand, "@BirthDate", DbType.DateTime, model.BirthDate);
                    sqlDatabase.AddInParameter(dbCommand, "@Age", DbType.Int32, model.Age);
                    sqlDatabase.AddInParameter(dbCommand, "@IsActive", DbType.Boolean, model.IsActive);
                    sqlDatabase.AddInParameter(dbCommand, "@Gender", DbType.String, model.Gender);
                    sqlDatabase.AddInParameter(dbCommand, "@Password", DbType.String, model.Password);
                    sqlDatabase.ExecuteNonQuery(dbCommand);
                    return true;
                }
            }
            catch(Exception ex)
            {
                return false;
            }
        }
        #endregion
    }
}
