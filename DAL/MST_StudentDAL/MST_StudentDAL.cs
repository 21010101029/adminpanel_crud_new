﻿using AdminPanel_New.Areas.Branch.Models;
using AdminPanel_New.Areas.City.Models;
using AdminPanel_New.Areas.Student.Models;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data;
using System.Data.Common;

namespace AdminPanel_New.DAL.MST_StudentDAL
{
    public class MST_StudentDAL:MST_StudentDALBase
    {

        #region BranchComboBox
        public List<MST_BranchModel> PR_Branch_ComboBox()
        {
            try {

                SqlDatabase sqlDatabase = new SqlDatabase(myConnectionString);
                DbCommand dbCommand = sqlDatabase.GetStoredProcCommand("PR_Branch_ComboBox");
                DataTable dt=new DataTable();
                using (IDataReader reader = sqlDatabase.ExecuteReader(dbCommand)) { 
                    dt.Load(reader);
                }

                List<MST_BranchModel> modelList= new List<MST_BranchModel>();
                foreach (DataRow dr in dt.Rows)     
                {
                    MST_BranchModel br = new MST_BranchModel();

                    br.BranchName = dr["BranchName"].ToString();
                    br.BranchID = Convert.ToInt32(dr["BranchID"]);
                    modelList.Add(br);
                }
                return modelList;
            }
            catch(Exception ex) {
                return null;
            }
        }
        #endregion

        #region CityComboBox
        public List<LOC_CityModel> PR_LOC_City_SelectComboBox()
        {
            try
            {

                SqlDatabase sqlDatabase = new SqlDatabase(myConnectionString);
                DbCommand dbCommand = sqlDatabase.GetStoredProcCommand("PR_LOC_City_SelectComboBox");
                DataTable dt = new DataTable();
                using (IDataReader reader = sqlDatabase.ExecuteReader(dbCommand))
                {
                    dt.Load(reader);
                }

                List<LOC_CityModel> modelList = new List<LOC_CityModel>();
                foreach (DataRow dr in dt.Rows)
                {
                    LOC_CityModel br = new LOC_CityModel();

                    br.CityName = dr["CityName"].ToString();
                    br.CityID = Convert.ToInt32(dr["CityID"]);
                    modelList.Add(br);
                }
                return modelList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        #endregion
    }
}
