﻿using AdminPanel_New.Areas.Country.Models;
using System.Data.Common;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace AdminPanel_New.DAL.LOC_CountryDAL
{
    public class LOC_CountryDALBase:DALHelper
    {
        #region dbo_PR_LOC_Country_SelectAll
        public DataTable dbo_PR_LOC_Country_SelectAll()
        {
            try
            {
                SqlDatabase sqlDatabase = new SqlDatabase(myConnectionString);
                DbCommand dbCommand = sqlDatabase.GetStoredProcCommand("PR_LOC_Country_SelectAll");
                DataTable dt = new DataTable();
                using (IDataReader reader = sqlDatabase.ExecuteReader(dbCommand))
                {
                    dt.Load(reader);
                }
                return dt;
            }
            catch (Exception e) { return null; }
        }
        #endregion

        #region dbo_PR_LOC_Country_Insert and dbo_PR_LOC_Country_Update
        public bool SaveCountry(LOC_CountryModel model)
        {
            try
            {
                if (model.CountryID == null)
                {
                    SqlDatabase sqlDatabase = new SqlDatabase(myConnectionString);
                    DbCommand dbCommand = sqlDatabase.GetStoredProcCommand("PR_LOC_Country_Insert");
                    sqlDatabase.AddInParameter(dbCommand, "@Created", DbType.DateTime, DBNull.Value);
                    sqlDatabase.AddInParameter(dbCommand, "@CountryName", DbType.String, model.CountryName);
                    sqlDatabase.AddInParameter(dbCommand, "@CountryCode", DbType.String, model.CountryCode);
                    sqlDatabase.AddInParameter(dbCommand, "@Modified", DbType.DateTime, DBNull.Value);
                    sqlDatabase.ExecuteNonQuery(dbCommand);
                    return true;
                }
                else
                {
                    SqlDatabase sqlDatabase = new SqlDatabase(myConnectionString);
                    DbCommand dbCommand = sqlDatabase.GetStoredProcCommand("PR_LOC_Country_Update");
                    sqlDatabase.AddInParameter(dbCommand, "@CountryID", DbType.Int64, model.CountryID);
                    sqlDatabase.AddInParameter(dbCommand, "@CountryName", DbType.String, model.CountryName);
                    sqlDatabase.AddInParameter(dbCommand, "@CountryCode", DbType.String, model.CountryCode);
                    sqlDatabase.AddInParameter(dbCommand, "@Modified", DbType.DateTime, DBNull.Value);
                    sqlDatabase.ExecuteNonQuery(dbCommand);
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }

        }
        #endregion

        #region dbo_PR_LOC_Country_SelectByPK
        public LOC_CountryModel PR_LOC_Country_SelectByPK(int? CountryID)
        {
            LOC_CountryModel modelLOC_CountryModel = new LOC_CountryModel();
            try
            {
                SqlDatabase sqlDatabase = new SqlDatabase(myConnectionString);
                DbCommand dbCommand = sqlDatabase.GetStoredProcCommand("PR_LOC_Country_SelectByPK");
                sqlDatabase.AddInParameter(dbCommand, "@CountryID", DbType.String, CountryID);
                DataTable dt = new DataTable();
                using (IDataReader reader = sqlDatabase.ExecuteReader(dbCommand))
                {
                    dt.Load(reader);
                }
                foreach (DataRow dr in dt.Rows)
                {
                    modelLOC_CountryModel.CountryName = dr["CountryName"].ToString();
                    modelLOC_CountryModel.Created = Convert.ToDateTime(dr["Created"].ToString());
                    modelLOC_CountryModel.Modified = Convert.ToDateTime(dr["Modified"].ToString());
                    modelLOC_CountryModel.CountryCode = dr["CountryCode"].ToString();
                    modelLOC_CountryModel.CountryID = Convert.ToInt32(dr["CountryID"]);

                }
                return modelLOC_CountryModel;
            }
            catch (Exception e) { return null; }
        }
        #endregion

        #region dbo_PR_LOC_Country_Delete
        public void PR_LOC_Country_Delete(int? CountryID)
        {
            try
            {
                SqlDatabase sqlDatabase = new SqlDatabase(myConnectionString);
                DbCommand dbCommand = sqlDatabase.GetStoredProcCommand("PR_LOC_Country_Delete");
                sqlDatabase.AddInParameter(dbCommand, "@CountryID", DbType.String, CountryID);
                sqlDatabase.ExecuteNonQuery(dbCommand);
                return;
            }
            catch (Exception e) { return; }
        }
        #endregion
    }
}
