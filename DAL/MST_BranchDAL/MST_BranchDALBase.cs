﻿using AdminPanel_New.Areas.Branch.Models;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data;
using System.Data.Common;

namespace AdminPanel_New.DAL.MST_BranchDAL
{
    public class MST_BranchDALBase:DALHelper
    {

        #region [dbo].[PR_Branch_SelectAll]
        public DataTable PR_Branch_SelectAll()
        {
            try
            {
                SqlDatabase sqlDatabase = new SqlDatabase(myConnectionString);
                DbCommand dbCommand = sqlDatabase.GetStoredProcCommand("PR_Branch_SelectAll");
                DataTable dt=new DataTable();
                using (IDataReader reader = sqlDatabase.ExecuteReader(dbCommand))
                {
                    dt.Load(reader);
                }
                return dt;  

            }catch (Exception ex)
            {
                return null;
            }
        }
        #endregion

        #region [dbo].[PR_Branch_UpdateByPK] & [dbo].[PR_Branch_SelectAll]
        public bool SaveBranch(MST_BranchModel model)
        {
            try
            {
                if (model.BranchID == 0)
                {
                    SqlDatabase sqlDatabase = new SqlDatabase(myConnectionString);
                    DbCommand dbCommand = sqlDatabase.GetStoredProcCommand("PR_Branch_Insert");
                    sqlDatabase.AddInParameter(dbCommand, "@BranchName", DbType.String, model.BranchName);
                    sqlDatabase.AddInParameter(dbCommand, "@BranchCode", DbType.String, model.BranchCode);
                    sqlDatabase.ExecuteNonQuery(dbCommand);
                    return true;
                }
                else
                {
                    SqlDatabase sqlDatabase = new SqlDatabase(myConnectionString);
                    DbCommand dbCommand = sqlDatabase.GetStoredProcCommand("PR_Branch_UpdateByPK");
                    sqlDatabase.AddInParameter(dbCommand, "@BranchID", DbType.Int64, model.BranchID);
                    sqlDatabase.AddInParameter(dbCommand, "@BranchName", DbType.String, model.BranchName);
                    sqlDatabase.AddInParameter(dbCommand, "@BranchCode", DbType.String, model.BranchCode);
                    sqlDatabase.ExecuteNonQuery(dbCommand);
                    return true;
                }

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        #endregion

        #region [dbo].[PR_Branch_SelectByPk]

        public MST_BranchModel PR_Branch_SelectByPk(int? branchID)
        {
            try
            {
                MST_BranchModel model = new MST_BranchModel();
                SqlDatabase sqlDatabase =new SqlDatabase(myConnectionString);
                DbCommand dbCommand = sqlDatabase.GetStoredProcCommand("PR_Branch_SelectByPk");
                sqlDatabase.AddInParameter(dbCommand, "@BranchID", DbType.Int64, branchID);
                DataTable dt = new DataTable();
                using ( IDataReader reader = sqlDatabase.ExecuteReader(dbCommand))
                {
                    dt.Load(reader);
                }

                foreach(DataRow dr in dt.Rows)
                {
                    model.BranchName = dr["BranchName"].ToString();
                    model.BranchID = Convert.ToInt32(dr["BranchID"]);
                    model.BranchCode= dr["BranchCode"].ToString();
                    model.Created = Convert.ToDateTime(dr["Created"].ToString());
                    model.Modified = Convert.ToDateTime(dr["Modified"].ToString());
                }
                return model;
            }
            catch (Exception ex) {
                return null;
            }
        }
        #endregion

        #region [dbo].[PR_Branch_DeleteByPK]
        public void PR_Branch_DeleteByPK(int? BranchID)
        {
            try
            {
                SqlDatabase sqlDatabase = new SqlDatabase(myConnectionString);
                DbCommand dbCommand = sqlDatabase.GetStoredProcCommand("PR_Branch_DeleteByPK");
                sqlDatabase.AddInParameter(dbCommand, "@BranchID", DbType.String, BranchID);
                sqlDatabase.ExecuteNonQuery(dbCommand);
                return;
            }
            catch (Exception e) { return; }
        }
        #endregion

    }
}
