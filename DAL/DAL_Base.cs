﻿using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;
using System.Data;
using AdminPanel_New.Models;

namespace AdminPanel_New.DAL
{
    public class DAL_Base:DALHelper
    {
        #region Search_By_Anything
        public DataTable Search_By_Anything(SearchModel model)
        {
            try
            {
                Console.WriteLine($"CountryId {model.CountryID}");
                Console.WriteLine($"StateID {model.StateID}");
                Console.WriteLine($"CityName {model.CityName}");

                SqlDatabase sqlDatabase = new SqlDatabase(myConnectionString);
                DbCommand dbCommand = sqlDatabase.GetStoredProcCommand("Search_By_Anything");
                sqlDatabase.AddInParameter(dbCommand, "@CName", DbType.String, model.CityName);
                sqlDatabase.AddInParameter(dbCommand, "@CountryID", DbType.Int64, model.CountryID);
                sqlDatabase.AddInParameter(dbCommand, "@StateID", DbType.Int64, model.StateID);
                DataTable dt = new DataTable();
                using (IDataReader reader = sqlDatabase.ExecuteReader(dbCommand))
                {
                    dt.Load(reader);
                }
                return dt;
            }
            catch (Exception e) { return null; }
        }
        #endregion
    }
}
