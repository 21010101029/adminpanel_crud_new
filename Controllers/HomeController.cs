﻿using AdminPanel_New.Models;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using System.Diagnostics;
using AdminPanel_New.DAL.LOC_CityDAL;
using AdminPanel_New.Areas.City.Models;
using AdminPanel_New.DAL.LOC_StateDAL;
using System.Collections.Generic;
using AdminPanel_New.DAL;

namespace AdminPanel_New.Controllers
{
    public class HomeController : Controller
    {
        LOC_CityDAL dalLOC_CityDAL = new LOC_CityDAL();
        LOC_StateDAL dalLOC_StateDAL   = new LOC_StateDAL();
        public IActionResult Index()
        {
            
            List<LOC_CityModel> list=new List<LOC_CityModel>();
            DataTable dt = dalLOC_CityDAL.dbo_PR_LOC_City_SelectAll();
            foreach (DataRow dr in dt.Rows)
            {
                LOC_CityModel model = new LOC_CityModel();
                model.StateName = dr["StateName"].ToString();
                model.StateCode = dr["StateCode"].ToString();
                model.Created = Convert.ToDateTime(dr["Created"].ToString());
                model.Modified = Convert.ToDateTime(dr["Modified"].ToString());
                model.CountryName = dr["CountryName"].ToString();
                model.CityID = Convert.ToInt32(dr["CityID"]);
                model.CityCode = dr["CityCode"].ToString();
                model.CityName = dr["CityName"].ToString();
                list.Add(model);
                
            }
            ViewBag.ListOfLocation=list;
            ViewBag.CountryList = dalLOC_StateDAL.DropdownCountry();
            ViewBag.StateList = dalLOC_CityDAL.DropDownState();
            //Console.WriteLine(list[0].)
            return View();
        }


        #region SearchLocation
        public IActionResult SearchLocation(SearchModel model)
        {
            DAL_Base dAL_Base = new DAL_Base();
            DataTable dt=dAL_Base.Search_By_Anything(model);
            List<SearchModel> list = new List<SearchModel>();
            foreach (DataRow dr in dt.Rows)
            {
                SearchModel model2 = new SearchModel();
                model2.StateName = dr["StateName"].ToString();
                model2.StateCode = dr["StateCode"].ToString();
                model2.Created = Convert.ToDateTime(dr["Created"].ToString());
                model2.Modified = Convert.ToDateTime(dr["Modified"].ToString());
                model2.CountryName = dr["CountryName"].ToString();
                model2.CityID = Convert.ToInt32(dr["CityID"]);
                model2.CityCode = dr["CityCode"].ToString();
                model2.CityName = dr["CityName"].ToString();
                list.Add(model2);

            }
            ViewBag.ListOfLocation = list;
            ViewBag.CountryList = dalLOC_StateDAL.DropdownCountry();
            ViewBag.StateList = dalLOC_CityDAL.DropDownState();

            return View("Index");
        }
        #endregion
        public IActionResult DropDownByCountryForStateInIndex(int CountryID)
        {
            var model = dalLOC_CityDAL.PR_LOC_State_SelectComboBoxByCountryID(CountryID);
            return Json(model);
        }

    }
}