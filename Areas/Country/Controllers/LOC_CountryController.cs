﻿using AdminPanel_New.Areas.Country.Models;
using AdminPanel_New.DAL.LOC_CountryDAL;
using AdminPanel_New.DAL.LOC_StateDAL;
using Microsoft.AspNetCore.Mvc;
using System.Data;

namespace AdminPanel_New.Areas.Country.Controllers
{
    [Area("Country")]
    [Route("Country/[controller]/[action]")]
    public class LOC_CountryController : Controller
    {
        LOC_CountryModel modelLOC_Country = new LOC_CountryModel();
        LOC_CountryDAL dalLOC_CountryDAL = new LOC_CountryDAL();

        #region LOC_CountryList Controller
        public IActionResult LOC_CountryList()
        {
            DataTable dt = dalLOC_CountryDAL.dbo_PR_LOC_Country_SelectAll();
            return View(dt);
        }
        #endregion

        #region Add NAvigator For Render PAge
        public IActionResult Add(int? CountryID)
        {


            LOC_CountryModel model = dalLOC_CountryDAL.PR_LOC_Country_SelectByPK(CountryID);
            if (model != null)
            {
                return View("AddEditCountry", model);
            }
            else
            {
                return View("AddEditCountry");
            }
        }
        #endregion

        #region Add /Edit Page Controller
        public IActionResult AddEditCountry(LOC_CountryModel modelLOC_CountryModel)
        {
            if (ModelState.IsValid)
            {
                if (dalLOC_CountryDAL.SaveCountry(modelLOC_CountryModel))
                {
                    if (modelLOC_CountryModel.CountryID == null)
                    {
                        TempData["CountryInsertMsg"] = "Record Inserted Successfully";
                        return RedirectToAction("LOC_CountryList");
                    }
                    else
                        return RedirectToAction("LOC_CountryList");
                }
            }
            return View("AddEditCountry");
        }
        #endregion

        #region Delete 
        public IActionResult Delete(int? CountryID)
        {
            dalLOC_CountryDAL.PR_LOC_deleteCity_FollowedBy_Country(CountryID);
            dalLOC_CountryDAL.PR_LOC_deleteState_FollowedBy_Country(CountryID);
            dalLOC_CountryDAL.PR_LOC_Country_Delete(CountryID);
            return RedirectToAction("LOC_CountryList");
        }
        #endregion

        #region SearchContry
        public IActionResult SearchContry(string CountryName)
        {
            DataTable dt = dalLOC_CountryDAL.PR_Country_SelectByCountryName(CountryName);
            return View("LOC_CountryList",dt);
        }
        #endregion
    }
}
