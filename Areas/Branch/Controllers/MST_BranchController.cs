﻿using AdminPanel_New.Areas.Branch.Models;
using AdminPanel_New.DAL.MST_BranchDAL;
using Microsoft.AspNetCore.Mvc;
using System.Data;

namespace AdminPanel_New.Areas.Branch.Controllers
{
    [Area("Branch")]
    [Route("Branch/[controller]/[action]")]
    public class MST_BranchController : Controller
    {
        MST_BranchDAL mst_BranchDAl=new MST_BranchDAL();

        #region BranchList
        public IActionResult MST_BranchList()
        {
            DataTable dt=mst_BranchDAl.PR_Branch_SelectAll();
            return View(dt);
        }

        #endregion

        #region Add NAvigator For Render PAge
        public IActionResult Add(int ? BranchID)
        {
            if(BranchID != null)
            {
                MST_BranchModel model = mst_BranchDAl.PR_Branch_SelectByPk(BranchID);
                return View("AddEditBranch",model);
            }
            else
            {
                return View("AddEditBranch");
            }

        }
        #endregion

        #region AddEditBranch
        public IActionResult AddEditBranch(MST_BranchModel model)
        {
            if (mst_BranchDAl.SaveBranch(model))
            {
                return RedirectToAction("MST_BranchList");
            }
            else
            {
                return View();
            }
            
        }
        #endregion

        #region Delete
        public IActionResult Delete(int ?BranchID)
        {
            Console.WriteLine(BranchID);

                mst_BranchDAl.PR_Branch_DeleteByPK(BranchID);
                return RedirectToAction("MST_BranchList");
        }
        #endregion
    }
}
