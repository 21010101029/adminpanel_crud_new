﻿

using System.ComponentModel.DataAnnotations;

namespace AdminPanel_New.Areas.Branch.Models
{
    public class MST_BranchModel
    {
        public int BranchID { get; set; }
        [Required]
        public string BranchName { get; set; }
        [Required]
        public string BranchCode { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
    }
}
