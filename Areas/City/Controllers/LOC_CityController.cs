﻿using AdminPanel_New.DAL.LOC_CityDAL;
using AdminPanel_New.DAL.LOC_StateDAL;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using AdminPanel_New.Areas.City.Models;

namespace AdminPanel_New.Areas.City.Controllers
{
    [Area("City")]
    [Route("City/[controller]/[action]")]
    public class LOC_CityController : Controller
    {
       
        LOC_CityDAL dalLOC_CityDAL= new LOC_CityDAL();
        LOC_StateDAL dalLOC_StateDAL = new LOC_StateDAL();

        #region LOC_CityList Controller
        public IActionResult LOC_CityList()
        {
            DataTable dt = dalLOC_CityDAL.dbo_PR_LOC_City_SelectAll();
            return View(dt);
        }
        #endregion

        #region ADD Method 
        public IActionResult Add(int ?CityID)
        {
            if (CityID == null)
            {
                ViewBag.CountryList = dalLOC_StateDAL.DropdownCountry();
                ViewBag.StateList = dalLOC_CityDAL.DropDownState();
                return View("AddEditCity");
            }
            else
            {
                LOC_CityModel model= dalLOC_CityDAL.PR_LOC_City_SelectByPK(CityID);
                ViewBag.CountryList = dalLOC_StateDAL.DropdownCountry();
                ViewBag.StateList = dalLOC_CityDAL.PR_LOC_State_SelectComboBoxByCountryID(model.CountryID);
                return View("AddEditCity",model);
            }
        }
        #endregion

        #region AddEditCity
        public IActionResult AddEditCity(LOC_CityModel model)
        {
            if (dalLOC_CityDAL.SaveCity(model))
            {
                return RedirectToAction("LOC_CityList");
            }
            return View("AddEditCity");
            
        }
        #endregion

        #region Delete
        public IActionResult Delete(int? CityID)
        {
            dalLOC_CityDAL.PR_LOC_City_Delete(CityID);
            return RedirectToAction("LOC_CityList");
        }
        #endregion

        #region DropDownByCountryForState
        public IActionResult DropDownByCountryForState(int CountryID)
        {
            var model=dalLOC_CityDAL.PR_LOC_State_SelectComboBoxByCountryID(CountryID);
            return Json(model);
        }

        #endregion

        #region SearchCity
        public IActionResult SearchCity(string CityName)
        {
            DataTable dt=dalLOC_CityDAL.PR_City_SelectByCityName(CityName);
            return View("LOC_CityList",dt);
        }
        #endregion
    }
}
