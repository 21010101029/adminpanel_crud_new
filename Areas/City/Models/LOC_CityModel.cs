﻿using System.ComponentModel.DataAnnotations;

namespace AdminPanel_New.Areas.City.Models
{
    public class LOC_CityModel
    {
        public int? CityID { get; set; }
        [Required]
        public int? CountryID { get; set; }
        [Required]
        public int? StateID { get; set; }
        [Required]
        public string CityName { get; set; }

        [Required]
        public string CityCode { get; set; } 
        public DateTime Created {get; set;}
        public DateTime Modified { get; set; }
        [Required]
        public string CountryName { get; set; }
        [Required]
        public string StateName { get; set; }
        public string StateCode { get; set; }
    }
    public class LOC_DropDownState
    {
        public int? StateID { get; set; }
        public string StateName { get; set; }
    }
}
