﻿using System.ComponentModel.DataAnnotations;

namespace AdminPanel_New.Areas.States.Models
{
    public class LOC_StateModel
    {
        public int StateID { get; set; }
        [Required]
        public string StateName { get; set; }
        [Required]
        public string StateCode { get; set; }
        [Required]
        public  int CountryID { get; set; }  
        public string CountryName { get; set; }
        public string CountryCode { get; set; }

        public DateTime? Created { get; set; }
        public DateTime? Modified { get; set; }
    }
}
