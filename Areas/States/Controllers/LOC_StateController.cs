﻿using AdminPanel_New.Areas.States.Models;
using AdminPanel_New.DAL.LOC_StateDAL;
using Microsoft.AspNetCore.Mvc;
using System.Data;

namespace AdminPanel_New.Areas.States.Controllers
{
    [Area("States")]
    [Route("States/[controller]/[action]")]
    public class LOC_StateController : Controller
    {
        LOC_StateDAL dalLOC_StateDAL = new LOC_StateDAL();
        #region LOC_StateList
        public IActionResult LOC_StateList()
        {
            List<LOC_StateModel> models = new List<LOC_StateModel>();
            DataTable dt = dalLOC_StateDAL.dbo_PR_LOC_State_SelectAll();
            foreach(DataRow dr in dt.Rows)
            {

            }
            ViewBag.CountryList = dalLOC_StateDAL.DropdownCountry();
            return View(dt);
        }
        #endregion

        #region ADD
        public IActionResult Add(int ? StateID)
        {
            if(StateID == null)
            {
                ViewBag.CountryList = dalLOC_StateDAL.DropdownCountry();
                return View("AddEditState");
            }
            else
            {
                LOC_StateModel model= dalLOC_StateDAL.PR_LOC_State_SelectByPK(StateID);
                ViewBag.CountryList = dalLOC_StateDAL.DropdownCountry();
                return View("AddEditState",model);
            }

        }
        #endregion

        #region ADD & Edit State
        public IActionResult AddEditState(LOC_StateModel model)
        {
            LOC_StateDAL dalLOC_StateDAL=new LOC_StateDAL();

            if (dalLOC_StateDAL.SaveState(model))
            {
                return RedirectToAction("LOC_StateList");
            }
            
            return View();
        }
        #endregion

        #region Delete
        public IActionResult Delete(int ? StateID) {
            dalLOC_StateDAL.PR_LOC_deleteCity_FollowedBy_States(StateID);
            dalLOC_StateDAL.PR_LOC_State_Delete(StateID);
            return RedirectToAction("LOC_StateList");
        }
        #endregion

        #region SearchState
        public IActionResult SearchState(string StateName)
        {
            DataTable dt = dalLOC_StateDAL.PR_State_SelectByStateName(StateName);
            return View("LOC_StateList", dt);
        }
        #endregion
    }
}
