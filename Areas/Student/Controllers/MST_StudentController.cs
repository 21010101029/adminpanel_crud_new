﻿using AdminPanel_New.Areas.States.Models;
using AdminPanel_New.Areas.Student.Models;
using AdminPanel_New.DAL.MST_StudentDAL;
using Microsoft.AspNetCore.Mvc;
using System.Data;

namespace AdminPanel_New.Areas.Student.Controllers
{
    [Area("Student")]
    [Route("Student/[controller]/[action]")]
    public class MST_StudentController : Controller
    {
        MST_StudentDAL mst_StudentDAL=new MST_StudentDAL();

        #region MST_StudentList
        public IActionResult MST_StudentList()
        {
            DataTable dt = mst_StudentDAL.PR_Student_SelectAll();
            return View(dt);
        }
        #endregion

        #region Add
        public IActionResult Add(int ?StudentID)
        {
            if (StudentID == null)
            {
                ViewBag.BranchList = mst_StudentDAL.PR_Branch_ComboBox();
                ViewBag.CityList = mst_StudentDAL.PR_LOC_City_SelectComboBox();
                return View("AddEditStudent");
            }
            else
            {
                ViewBag.BranchList = mst_StudentDAL.PR_Branch_ComboBox();
                ViewBag.CityList = mst_StudentDAL.PR_LOC_City_SelectComboBox();
                MST_StudentModel model=mst_StudentDAL.PR_Student_SelectByPk(StudentID);
                return View("AddEditStudent",model);
            }
        }

        #endregion

        #region AddEditStudent
        public IActionResult AddEditStudent(MST_StudentModel model)
        {
            if (mst_StudentDAL.Save(model))
            {
                return RedirectToAction("MST_StudentList");
            }

            return View();
        }

        #endregion

        #region Delete
        public IActionResult Delete(int StudentID)
        {
            mst_StudentDAL.PR_Student_DeleteByPK(StudentID);
            return RedirectToAction("MST_StudentList");  
        }
        #endregion
    }
}

